import * as React from "react";
import HeaderSection  from "./../sections/headerSection";
import AboutSection  from "./../sections/aboutSection";
import HeroSection from "./../sections/heroSection";
import HowItWorksSection from "./../sections/howItWorksSection";
import CtaSection from "./../sections/ctaSection";
import ComunitySection from "./../sections/comunitySection";
import ProductsSection from "./../sections/productsSection";
import QuestionsSection from "./../sections/questionsSection";
import CustomSection from "./../sections/customSection";
import CollabSection from "./../sections/collabSection";
import BussinesAnalystSection from "./../sections/bussinesAnalystSection";
import FaqSection from "./../sections/faqSection";
import FooterSection from "./../sections/footerSection";
import "./../styles/global.css";


// markup
const IndexPage = () => {
  return (
    <main>
      <title>Tarefas & Processos</title>
      <HeaderSection/>
      <HeroSection/>
      <AboutSection/>
      <HowItWorksSection/>
      <CtaSection/>
      <ComunitySection/>
      <ProductsSection/>
      <QuestionsSection/>
      <CustomSection/>
      <CollabSection/>
      <BussinesAnalystSection/>
      <FaqSection/>
      <FooterSection/>
    </main>
  )
}

export default IndexPage
