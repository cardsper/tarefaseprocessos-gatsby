import * as React from "react";
import Img from "./../images/resume-amico.svg";
function comunitySection(){
    return(
        <section className="text-gray-600 body-font">
        <div className="container flex flex-col items-center px-5 py-24 mx-auto md:flex-row">
          <div className="w-5/6 mb-10 lg:max-w-lg lg:w-full md:w-1/2 md:mb-0">
            <img className="object-cover object-center rounded" alt="hero" src={Img}/>
          </div>
          <div className="flex flex-col items-center text-center lg:flex-grow md:w-1/2 lg:pl-24 md:pl-16 md:items-start md:text-left">
            <h1 className="mb-4 text-3xl font-medium text-gray-900 title-font sm:text-4xl">Aprenda com quem já fez
              <br className="hidden lg:inline-block"/>saiba por onde começar e o que fazer
            </h1>
            <p className="mb-8 leading-relaxed">Copper mug try-hard pitchfork pour-over freegan heirloom neutra air plant cold-pressed tacos poke beard tote bag. Heirloom echo park mlkshk tote bag selvage hot chicken authentic tumeric truffaut hexagon try-hard chambray.</p>
          </div>
        </div>
      </section>
    )
}

export default comunitySection;