import * as React from "react";
function ProductsSection() {
    return(
<section id="products-and-services" className="text-gray-600 body-font">
  <div className="container px-5 py-24 mx-auto">
    <div className="flex flex-col flex-wrap items-center w-full mb-20 text-center">
      <h1 className="mb-2 text-2xl font-medium text-gray-900 sm:text-3xl title-font">Produtos & Serviços</h1>
      <p className="w-full leading-relaxed text-gray-500 lg:w-1/2">Veniam eu adipisicing adipisicing ullamco. Ad elit mollit est et pariatur anim dolore mollit.</p>
    </div>
    <div className="flex flex-wrap -m-4">
      <div className="p-4 xl:w-1/3 md:w-1/2">
        <div className="p-6 border border-gray-200 rounded-lg">
          <div className="inline-flex items-center justify-center w-10 h-10 mb-4 text-indigo-500 bg-indigo-100 rounded-full">
            <svg fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" className="w-6 h-6" viewBox="0 0 24 24">
              <path d="M22 12h-4l-3 9L9 3l-3 9H2"></path>
            </svg>
          </div>
          <h2 className="mb-2 text-lg font-medium text-gray-900 title-font">Mapeamento e modelagem de processos</h2>
          <p className="text-base leading-relaxed">Laboris nostrud nisi do culpa pariatur ex. Aute minim excepteur ipsum labore et mollit ea occaecat duis ipsum.</p>
        </div>
      </div>
      <div className="p-4 xl:w-1/3 md:w-1/2">
        <div className="p-6 border border-gray-200 rounded-lg">
          <div className="inline-flex items-center justify-center w-10 h-10 mb-4 text-indigo-500 bg-indigo-100 rounded-full">
            <svg fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" className="w-6 h-6" viewBox="0 0 24 24">
              <circle cx="6" cy="6" r="3"></circle>
              <circle cx="6" cy="18" r="3"></circle>
              <path d="M20 4L8.12 15.88M14.47 14.48L20 20M8.12 8.12L12 12"></path>
            </svg>
          </div>
          <h2 className="mb-2 text-lg font-medium text-gray-900 title-font">Base de conhecimento</h2>
          <p className="text-base leading-relaxed">Culpa qui eiusmod est cillum minim ex. Deserunt aute ad incididunt proident quis eu eu dolor aliqua nostrud.</p>
        </div>
      </div>
      <div className="p-4 xl:w-1/3 md:w-1/2">
        <div className="p-6 border border-gray-200 rounded-lg">
          <div className="inline-flex items-center justify-center w-10 h-10 mb-4 text-indigo-500 bg-indigo-100 rounded-full">
            <svg fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" className="w-6 h-6" viewBox="0 0 24 24">
              <path d="M20 21v-2a4 4 0 00-4-4H8a4 4 0 00-4 4v2"></path>
              <circle cx="12" cy="7" r="4"></circle>
            </svg>
          </div>
          <h2 className="mb-2 text-lg font-medium text-gray-900 title-font">Simulação de processos e avaliação de desempenho</h2>
          <p className="text-base leading-relaxed">In est tempor esse deserunt velit ut commodo minim anim voluptate nulla eu qui Lorem.
          </p>
        </div>
      </div>
      <div className="p-4 xl:w-1/3 md:w-1/2">
        <div className="p-6 border border-gray-200 rounded-lg">
          <div className="inline-flex items-center justify-center w-10 h-10 mb-4 text-indigo-500 bg-indigo-100 rounded-full">
            <svg fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" className="w-6 h-6" viewBox="0 0 24 24">
              <path d="M4 15s1-1 4-1 5 2 8 2 4-1 4-1V3s-1 1-4 1-5-2-8-2-4 1-4 1zM4 22v-7"></path>
            </svg>
          </div>
          <h2 className="mb-2 text-lg font-medium text-gray-900 title-font">Colaboração</h2>
          <p className="text-base leading-relaxed">Enim in dolor magna nostrud nulla anim. Do et non elit ea. Mollit nostrud aute nostrud dolor aute.</p>
        </div>
      </div>
      <div className="p-4 xl:w-1/3 md:w-1/2">
        <div className="p-6 border border-gray-200 rounded-lg">
          <div className="inline-flex items-center justify-center w-10 h-10 mb-4 text-indigo-500 bg-indigo-100 rounded-full">
            <svg fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" className="w-6 h-6" viewBox="0 0 24 24">
              <path d="M21 12.79A9 9 0 1111.21 3 7 7 0 0021 12.79z"></path>
            </svg>
          </div>
          <h2 className="mb-2 text-lg font-medium text-gray-900 title-font">Desenvolvimento de sistemas e workflow</h2>
          <p className="text-base leading-relaxed">Minim labore ea laborum aliquip ut. Nostrud Lorem et aliquip excepteur magna quis aliquip.</p>
        </div>
      </div>
      
    </div>
    <button className="flex px-8 py-2 mx-auto mt-16 text-lg text-white bg-indigo-500 border-0 rounded focus:outline-none hover:bg-indigo-600">Assine Agora</button>
  </div>
</section>
    )
}

export default ProductsSection;