import * as React from "react";
import Img from "./../images/setup-amico.svg";
function AboutSection() {
  return (
    <section id="about" className="text-gray-600 body-font">
  <div className="container flex flex-col items-center px-5 py-24 mx-auto md:flex-row">
    <div className="w-5/6 mb-10 lg:max-w-lg lg:w-full md:w-1/2 md:mb-0">
      <img className="object-cover object-center rounded" alt="hero" src={Img}/>
    </div>
    <div className="flex flex-col items-center text-center lg:flex-grow md:w-1/2 lg:pl-24 md:pl-16 md:items-start md:text-left">
      <h1 className="mb-4 text-3xl font-medium text-gray-900 title-font sm:text-4xl">O que é a plataforma 
        <br className="hidden lg:inline-block"/>Tarefas e Processos?
      </h1>
      <p className="mb-8 leading-relaxed">Culpa esse ea Lorem dolor eu aliqua nulla elit ex ullamco pariatur ipsum sint quis. Voluptate aliqua eu reprehenderit labore nostrud mollit Lorem reprehenderit enim adipisicing. Reprehenderit ut non adipisicing sint Lorem elit enim ex deserunt id.</p>
      <div className="flex justify-center">
        <button className="inline-flex px-6 py-2 text-lg text-white bg-indigo-500 border-0 rounded focus:outline-none hover:bg-indigo-600">Cadastre-se</button>
        <button className="inline-flex px-6 py-2 ml-4 text-lg text-gray-700 bg-gray-100 border-0 rounded focus:outline-none hover:bg-gray-200">Saiba mais</button>
      </div>
    </div>
  </div>
</section>
  );
}

export default AboutSection;
