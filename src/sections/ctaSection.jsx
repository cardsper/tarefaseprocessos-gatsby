import * as React from "react";
function CtaSection(){
return(
<section className="text-indigo-200 bg-indigo-800 body-font">
  <div className="container px-5 py-24 mx-auto">
    <div className="flex flex-col items-start flex-grow mx-auto lg:w-2/3 sm:flex-row sm:items-center">
      <div>
        <h1 className="mb-4 text-4xl font-medium text-white sm:pr-16 title-font sm:text-4xl">Saiba em detalhes como realizar alguma tarefa e resolver um problema específico. </h1>
        <p>
          Na prática, tudo que acontece no seu dia a dia é, na verdade, uma sequência de atividades que formam um processo. Realize seu cadastro e deixe o resto com a equipe do Tarefas & Processos!
        </p>
      </div>
      <button className="flex-shrink-0 px-8 py-2 mt-10 text-lg text-white bg-indigo-600 border-0 rounded focus:outline-none hover:bg-indigo-700 sm:mt-0">Comece Agora</button>
    </div>
  </div>
</section>
)
}

export default CtaSection;