import * as React from "react";
import SwiperCore, { Navigation, Pagination, Scrollbar, A11y } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";
import Img1 from "./../images/multitasking-amico.svg";
import Img2 from "./../images/ok-amico.svg";
import Img3 from "./../images/analytics-amico.svg";

import "swiper/swiper.scss";
import "swiper/components/navigation/navigation.scss";
import "swiper/components/pagination/pagination.scss";
import "swiper/components/scrollbar/scrollbar.scss";

SwiperCore.use([Navigation, Pagination, Scrollbar, A11y]);

function HeroSection() {
  const carouselData = [
    {
      title:
        "O melhor meio de instruções para solucionar problemas específicos.",
      description:
        "A melhor base de conhecimento é a que fornece informações completas, com instruções já testadas.",
      img: Img1,
    },
    {
      title: "A melhor forma de fazer é fazendo a coisa certa.",
      description:
        "Siga nossas orientações práticas e delibere resultados rapidamente. Acompanhamos o seu proceso oferecendo suporte sem complexidade.",
      img: Img2,
    },
    {
      title: "Você não precisar mais ficar achando nada.",
      description:
        "Coloque a prova seu conceito: faça, mensure,  erre, aprenda e refaça.",
      img: Img3,
    },
  ];

  return (
    <Swiper
      navigation={true}
      pagination={{ clickable: true }}
      autoplay={true}
      className="flex bg-indigo-900"
    >
      {carouselData.map((data, index) => (
        <SwiperSlide
          key={index}
          className="flex flex-col items-center justify-center px-5 py-8 mx-auto lg:px-40 md:flex-row"
        >
          <div className="flex flex-col items-center text-center lg:w-1/2 lg:pr-24 md:items-start md:text-left md:mb-0">
            <h1 className="mb-4 text-3xl font-medium text-white lg:text-5xl">
              {data.title}
            </h1>
            <p className="mb-8 leading-relaxed text-indigo-200">
              {data.description}
            </p>
            <div className="flex justify-center">
              <button className="inline-flex px-6 py-2 text-lg text-white bg-indigo-600 border-0 rounded focus:outline-none hover:bg-indigo-700">
                Saiba Mais
              </button>
              <button className="inline-flex px-6 py-2 ml-4 text-lg bg-gray-100 border-0 rounded focus:outline-none hover:bg-indigo-100">
                Cadastre-se
              </button>
            </div>
          </div>
          <div className="justify-center hidden w-full lg:w-1/2 lg:flex">
            <img className="mx-auto" alt="hero" src={data.img} />
          </div>
        </SwiperSlide>
      ))}
    </Swiper>
  );
}

export default HeroSection;
