import * as React from "react";
function CustomSection() {
    return(
        <section id="custom" className="text-gray-600 body-font">
  <div className="container flex flex-wrap items-center px-5 py-24 mx-auto">
    <div className="pb-10 mb-10 border-b border-gray-200 md:w-1/2 md:pr-12 md:py-8 md:border-r md:border-b-0 md:mb-0">
      <h1 className="mb-2 text-2xl font-medium text-gray-900 sm:text-3xl title-font">Precisa de um Mapeamento específico?</h1>
      <p className="text-base leading-relaxed">Contrate apenas o mapeamento detalhado do processo específico e personalizado sobre medida que você necessita. Confira algumas categorias.</p>
      <a href="#test" className="inline-flex items-center mt-4 text-indigo-500">Saiba Mais
        <svg fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" className="w-4 h-4 ml-2" viewBox="0 0 24 24">
          <path d="M5 12h14M12 5l7 7-7 7"></path>
        </svg>
      </a>
    </div>
    <div className="flex flex-col md:w-1/2 md:pl-12">
      <h2 className="mb-3 text-sm font-semibold tracking-wider text-gray-800 title-font">CATEGORIAS</h2>
      <nav className="flex flex-col flex-wrap -mb-1 list-none">
        <li className="mb-4">
          <a href="#test" className="text-indigo-400 hover:text-indigo-500">Mapeamento, modelagem, redesenho e simulação</a>
        </li>
        <li className="mb-4">
          <a href="#test" className="text-indigo-400 hover:text-indigo-500">Artefatos e documentos </a>
        </li>
        <li className="mb-4">
          <a href="#test" className="text-indigo-400 hover:text-indigo-500">Automação de processos</a>
        </li>
        <li className="mb-4">
          <a href="#test" className="text-indigo-400 hover:text-indigo-500">Pacote de horas</a>
        </li>
      </nav>
    </div>
  </div>
</section>
    )
    
}

export default CustomSection;