import * as React from "react";
import LOGO from "./../images/logo.svg";

function FooterSection() {
    return(
<footer className="text-gray-600 body-font">
  <div className="container flex flex-col items-center px-5 py-8 mx-auto sm:flex-row">
    <a href="#link" className="flex items-center justify-center font-medium text-gray-900 title-font md:justify-start">
      <img className="ml-3" src={LOGO} alt="" />
    </a>
    <p className="mt-4 text-sm text-gray-500 sm:ml-4 sm:pl-4 sm:border-l-2 sm:border-gray-200 sm:py-2 sm:mt-0">© 2021 Tarefas & Processos —
      <a href="#Evolpen" className="ml-1 text-gray-600" rel="noopener noreferrer" target="_blank">@Evolpen</a>
    </p>
    <span className="inline-flex justify-center mt-4 sm:ml-auto sm:mt-0 sm:justify-start">
      <a href="#link" className="text-gray-500">
        <svg fill="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" className="w-5 h-5" viewBox="0 0 24 24">
          <path d="M18 2h-3a5 5 0 00-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 011-1h3z"></path>
        </svg>
      </a>
      <a href="#link" className="ml-3 text-gray-500">
        <svg fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" className="w-5 h-5" viewBox="0 0 24 24">
          <rect width="20" height="20" x="2" y="2" rx="5" ry="5"></rect>
          <path d="M16 11.37A4 4 0 1112.63 8 4 4 0 0116 11.37zm1.5-4.87h.01"></path>
        </svg>
      </a>
    </span>
  </div>
</footer>
    )
}

export default FooterSection;