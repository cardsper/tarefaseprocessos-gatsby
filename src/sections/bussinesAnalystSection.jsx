import * as React from "react";
function BussinesAnalystSection() {
    return(
<section id="bussines-analyst" className="text-gray-600 body-font">
  <div className="container px-5 py-24 mx-auto">
    <div className="flex flex-col w-full mb-12 text-center">
      <h1 className="mb-4 text-2xl font-medium text-gray-900 sm:text-3xl title-font">Sou analista de negócio, quero fazer parte do banco se talentos </h1>
      <p className="mx-auto text-base leading-relaxed lg:w-2/3">Preencha o formulário abaixo e envie uma solicitação para fazer parte do nosso banco de talentos imediatamente. Enviaremos um email de confirmação em segundos.</p>
    </div>
    <div className="flex flex-col items-end w-full px-8 mx-auto space-y-4 lg:w-2/3 sm:flex-row sm:space-x-4 sm:space-y-0 sm:px-0">
      <div className="relative flex-grow w-full">
        <label htmlFor="full-name" className="text-sm leading-7 text-gray-600">Nome Completo</label>
        <input type="text" id="full-name" name="full-name" className="w-full px-3 py-1 text-base leading-8 text-gray-700 transition-colors duration-200 ease-in-out bg-gray-100 bg-opacity-50 border border-gray-300 rounded outline-none focus:border-indigo-500 focus:bg-transparent focus:ring-2 focus:ring-indigo-200"/>
      </div>
      <div className="relative flex-grow w-full">
        <label htmlFor="email" className="text-sm leading-7 text-gray-600">E-mail</label>
        <input type="email" id="email" name="email" className="w-full px-3 py-1 text-base leading-8 text-gray-700 transition-colors duration-200 ease-in-out bg-gray-100 bg-opacity-50 border border-gray-300 rounded outline-none focus:border-indigo-500 focus:bg-transparent focus:ring-2 focus:ring-indigo-200"/>
      </div>
      <button className="px-8 py-2 text-lg text-white bg-indigo-500 border-0 rounded focus:outline-none hover:bg-indigo-600">Enviar</button>
    </div>
  </div>
</section>
    )
}

export default BussinesAnalystSection;