import * as React from "react";
import NavMenu from "../components/navMenu";

function HeaderSection() {
  return (
    <div className="sticky top-0 z-50 text-gray-600 bg-white body-font">
      <NavMenu />
    </div>
  );
}

export default HeaderSection;
