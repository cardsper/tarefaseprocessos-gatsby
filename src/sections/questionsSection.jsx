import * as React from "react";
function QuestionSection() {
    return(
<section id="questions" className="text-gray-600 body-font">
  <div className="container px-5 py-24 mx-auto">
    <div className="mb-20 text-center">
      <h1 className="mb-4 text-2xl font-medium text-center text-gray-900 sm:text-3xl title-font">Quero comprar um processo já documentado.
<br />O que vou receber?</h1>
      <p className="mx-auto text-base leading-relaxed xl:w-2/4 lg:w-3/4">Est Lorem consectetur sint mollit mollit incididunt ipsum voluptate adipisicing duis anim minim quis ea. Quis cillum excepteur deserunt cupidatat magna cupidatat.</p>
    </div>
    <div className="flex flex-wrap -mx-2 lg:w-4/5 sm:mx-auto sm:mb-2">
      <div className="w-full p-2 sm:w-1/2">
        <div className="flex items-center h-full p-4 bg-gray-100 rounded">
          <svg fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="3" className="flex-shrink-0 w-6 h-6 mr-4 text-indigo-500" viewBox="0 0 24 24">
            <path d="M22 11.08V12a10 10 0 11-5.93-9.14"></path>
            <path d="M22 4L12 14.01l-3-3"></path>
          </svg>
          <span className="font-medium title-font">Mapa / Modelagem de Processo</span>
        </div>
      </div>
      <div className="w-full p-2 sm:w-1/2">
        <div className="flex items-center h-full p-4 bg-gray-100 rounded">
          <svg fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="3" className="flex-shrink-0 w-6 h-6 mr-4 text-indigo-500" viewBox="0 0 24 24">
            <path d="M22 11.08V12a10 10 0 11-5.93-9.14"></path>
            <path d="M22 4L12 14.01l-3-3"></path>
          </svg>
          <span className="font-medium title-font">Sugestão de Ferramentas e Sistemas</span>
        </div>
      </div>
      <div className="w-full p-2 sm:w-1/2">
        <div className="flex items-center h-full p-4 bg-gray-100 rounded">
          <svg fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="3" className="flex-shrink-0 w-6 h-6 mr-4 text-indigo-500" viewBox="0 0 24 24">
            <path d="M22 11.08V12a10 10 0 11-5.93-9.14"></path>
            <path d="M22 4L12 14.01l-3-3"></path>
          </svg>
          <span className="font-medium title-font">Detalhamento do Processo</span>
        </div>
      </div>
      <div className="w-full p-2 sm:w-1/2">
        <div className="flex items-center h-full p-4 bg-gray-100 rounded">
          <svg fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="3" className="flex-shrink-0 w-6 h-6 mr-4 text-indigo-500" viewBox="0 0 24 24">
            <path d="M22 11.08V12a10 10 0 11-5.93-9.14"></path>
            <path d="M22 4L12 14.01l-3-3"></path>
          </svg>
          <span className="font-medium title-font">Dica de execução</span>
        </div>
      </div>
      <div className="w-full p-2 sm:w-1/2">
        <div className="flex items-center h-full p-4 bg-gray-100 rounded">
          <svg fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="3" className="flex-shrink-0 w-6 h-6 mr-4 text-indigo-500" viewBox="0 0 24 24">
            <path d="M22 11.08V12a10 10 0 11-5.93-9.14"></path>
            <path d="M22 4L12 14.01l-3-3"></path>
          </svg>
          <span className="font-medium title-font">Simulação de Desempenho </span>
        </div>
      </div>
      <div className="w-full p-2 sm:w-1/2">
        <div className="flex items-center h-full p-4 bg-gray-100 rounded">
          <svg fill="none" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="3" className="flex-shrink-0 w-6 h-6 mr-4 text-indigo-500" viewBox="0 0 24 24">
            <path d="M22 11.08V12a10 10 0 11-5.93-9.14"></path>
            <path d="M22 4L12 14.01l-3-3"></path>
          </svg>
          <span className="font-medium title-font">Wiki (Banco de Processos)
</span>
        </div>
      </div>
    </div>
  </div>
</section>
    )
}

export default QuestionSection;