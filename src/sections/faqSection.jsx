import * as React from "react";
function FaqSection() {
    return(
<section className="overflow-hidden text-gray-600 body-font">
  <div className="px-5 py-24 mx-auto lg:w-1/2">
  <div className="flex flex-col flex-wrap items-center w-full mb-20 text-center">
      <h1 className="mb-2 text-3xl font-medium text-gray-900 sm:text-4xl title-font">Perguntas Frequentes</h1>
    </div>
    <div className="-my-8 divide-y-2 divide-gray-100">
      <div className="flex flex-wrap py-8 md:flex-nowrap">
        <div className="md:flex-grow">
          <h2 className="mb-2 text-2xl font-medium text-gray-900 title-font">Bitters hashtag waistcoat fashion axe chia unicorn</h2>
          <p className="leading-relaxed">Glossier echo park pug, church-key sartorial biodiesel vexillologist pop-up snackwave ramps cornhole. Marfa 3 wolf moon party messenger bag selfies, poke vaporware kombucha lumbersexual pork belly polaroid hoodie portland craft beer.</p>
        </div>
      </div>
      <div className="flex flex-wrap py-8 md:flex-nowrap">
        <div className="md:flex-grow">
          <h2 className="mb-2 text-2xl font-medium text-gray-900 title-font">Meditation bushwick direct trade taxidermy shaman</h2>
          <p className="leading-relaxed">Glossier echo park pug, church-key sartorial biodiesel vexillologist pop-up snackwave ramps cornhole. Marfa 3 wolf moon party messenger bag selfies, poke vaporware kombucha lumbersexual pork belly polaroid hoodie portland craft beer.</p>
        </div>
      </div>
      <div className="flex flex-wrap py-8 md:flex-nowrap">
        <div className="md:flex-grow">
          <h2 className="mb-2 text-2xl font-medium text-gray-900 title-font">Woke master cleanse drinking vinegar salvia</h2>
          <p className="leading-relaxed">Glossier echo park pug, church-key sartorial biodiesel vexillologist pop-up snackwave ramps cornhole. Marfa 3 wolf moon party messenger bag selfies, poke vaporware kombucha lumbersexual pork belly polaroid hoodie portland craft beer.</p>
        </div>
      </div>
    </div>
  </div>
</section>
    )
}

export default FaqSection;