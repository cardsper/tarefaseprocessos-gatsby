import * as React from "react";
// import { SplideSlide } from "@splidejs/react-splide";

function HeroSplide(props) {
  return (
      <SplideSlide className="flex bg-indigo-900">
      
<div className="container flex flex-col items-center justify-center px-5 py-8 mx-auto lg:px-40 md:flex-row">
  <div className="flex flex-col items-center text-center lg:pr-24 md:items-start md:text-left md:mb-0">
    <h1 className="mb-4 text-3xl font-medium text-white lg:text-5xl">{props.title}</h1>
    <p className="mb-8 leading-relaxed text-indigo-200">{props.description}</p>
    <div className="flex justify-center">
      <button className="inline-flex px-6 py-2 text-lg text-white bg-indigo-600 border-0 rounded focus:outline-none hover:bg-indigo-700">Saiba Mais</button>
      <button className="inline-flex px-6 py-2 ml-4 text-lg bg-gray-100 border-0 rounded focus:outline-none hover:bg-indigo-100">Cadastre-se</button>
    </div>
  </div>
  <div className="hidden w-full lg:block">
    <img alt="hero" src={props.img}/>
  </div>
</div>
      </SplideSlide>
  )
}

export default HeroSplide;
