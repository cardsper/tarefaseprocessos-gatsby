import React, { useState } from "react";
import Logo from "./../images/logo.svg";
import LogoIsotype from "./../images/logo-isotype.svg";
import { Transition } from "@headlessui/react";

function NavMenu() {
  const links = [
    { href: "#about", text: "Sobre" },
    { href: "#how-it-works", text: "Como funciona?" },
    { href: "#products-and-services", text: "Produtos & Serviços" },
    { href: "#questions", text: "Dúvidas" },
    { href: "#custom", text: "Personalizado" },
    { href: "#collab", text: "Colaboradores" },
    { href: "#bussines-analyst", text: "Analistas de negócio" },
  ];

  const [isOpen, setIsOpen] = useState(false);

  const listItems = links.map((link, index) => (
    <a
      key={index}
      href={link.href}
      className="py-4 text-indigo-400 lg:py-0 lg:mx-3 md:mx-1 hover:text-indigo-500"
    >
      {link.text}
    </a>
  ));

  function NavButton() {
    return (
      <button
        className="block p-1 lg:hidden"
        onClick={() => setIsOpen(!isOpen)}
      >
        <svg
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 24 24"
          height="24"
          width="24"
          stroke="currentColor"
          aria-hidden="true"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth="3"
            d={!isOpen ? "M4 6h16M4 12h16M4 18h16" : "M6 18L18 6M6 6l12 12"}
          />
        </svg>
      </button>
    );
  }

  return (
    <nav>
      <Transition
      show={isOpen}
      enter="transition ease-in-out duration-75 transform"
        enterFrom="translate-x-full"
        enterTo="translate-x-0"
        leave="transition ease-in-out duration-75 transform"
        leaveFrom="translate-x-0"
        leaveTo="translate-x-full"
      >
        <div
          style={{
            backdropFilter: "blur(50px)",
            backgroundColor: "rgba(255, 255, 255, 0.75)",
          }}
          className="absolute right-0 z-50 flex flex-col h-screen px-10 py-4 shadow-lg top-14 lg:hidden"
        >
          {listItems}
        </div>
      </Transition>

      <div className="sticky top-0 z-50 flex flex-row justify-between px-4 text-base bg-white shadow-md stop-0 lg:h-auto lg:py-6 xl:px-24 md:px-18 h-14 lg:justify-start">
        <div className="flex items-center lg:mr-3">
          <img className="block xl:block md:block sm:block lg:hidden" src={Logo} alt="" />
          <img className="hidden md:hidden sm:hidden lg:block xl:hidden" src={LogoIsotype} alt="" />
        </div>

        <div className="left-0 flex-row items-center flex-grow hidden lg:flex lg:border-l md:border-gray-200">
          {listItems}
        </div>

        <NavButton />
      </div>
    </nav>
  );
}

export default NavMenu;
