module.exports = {
  pathPrefix: "/tarefaseprocessos-gatsby",
  siteMetadata: {
    title: "tarefaseprocessos-gatsby",
  },
  plugins: [ 'gatsby-plugin-postcss', 'gatsby-plugin-sass',
    {
      resolve: "gatsby-plugin-manifest",
      options: {
        icon: "src/images/icon.png",
      },
    },
  ],
};
